/*
 * test_сompass.c
 *
 *  Created on: 8 окт. 2018 г.
 *      Author: rovbuilder
 */
#define  F_CPU 8000000L
#include <avr/interrupt.h>
#include <avr/io.h>
#include "ArcTAN.h"
#include <avr/iom8.h>
#include <util/delay.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <avr/eeprom.h>
#include "i2c.h"
#include "ADC.h"
#include "MPU6050.h"


//UART SPEED
#define BAUD 38400
#define UBRR_VAL F_CPU/16/BAUD-1


/////////////////////////////
float ACCEL_XANGLE;
float ACCEL_YANGLE;
float ACCEL_ZANGLE;
long int ACCEL;
long int pitch;
long int roll;
char pitch_h;
char pitch_l;
char roll_h;
char roll_l;
float Xfloat;
char status=0xBB;
char hit_counter=0;
char message_counter=0;


/////////////////////////////
char ADC_status=0;
volatile char DATA[29];// = "0123456789ABCDEFGJKLMNOPQR";
int buffer_index;
int buffer_MAX = 28;
int i=0;
/////////////////////////////
unsigned int min_X = 0xffff; //калибровка
unsigned int min_Y = 0xffff; //калибровка
unsigned int max_X = 0x0000; //калибровка
unsigned int max_Y = 0x0000;
unsigned int min_Z = 0xffff; //калибровка
unsigned int max_Z = 0x0000; //калибровка


/////////////////////////////////////////////////////////////////////////////




ISR (USART_UDRE_vect)
{
		// Увеличиваем индекс

if(buffer_index == buffer_MAX)  	// Вывааели весь буффер?
	{
	buffer_index=0;
	//message_counter++;
	//UCSRB &=~(1<<UDRIE);	// Запрещаем прерывание по опустошению - передача закончена
	}
	else
	{
	UDR = DATA	[buffer_index];
	//_delay_us(10);
	buffer_index ++;	// Берем данные из буффера.
	}
}

void usart_init (unsigned int speed)
{
	UBRRH=(unsigned char)(speed>>8);// Baud Rate
	UBRRL=(unsigned char) speed;// Baud Rate
	UCSRA=0x00;
	UCSRB|=(1<<TXEN)|(1<<RXEN)|(0<<RXCIE)|(0<<TXCIE);// Разрешение приёма и передачи
	UCSRB|=(1<<RXCIE);// Разрешение прерываний по приему
	UCSRC=(1<<UCSZ1)|(1<<UCSZ0)|(1<<URSEL);// Установка формата посылки: 8 бит данных, 1 стоп-бит
	//DDRB=0xFF;
	//DDRD=0xFF; //порт D работает как выход, так как к нему относятся выводы OC0A и OC0B
}

/////////////////////////////
void port_init(void)
{
DDRD|=0X08;//LIGHT2
DDRD|=0X20;//SET-RESET
PORTD|=0X22;
DDRB&=0XFD;
}

void init_devices(void)
{
 port_init();
 initMasterSPI();
 configADC();
 usart_init(UBRR_VAL);
 SREG|=0X80;
 i2c_init();
 Init_6050();
}

void UARTSend(long int data) {
	DDRD|=0X02;//TXD
	UDR=0XBB;// SOH
	while(!(UCSRA&(1<<UDRE)));
	_delay_ms(10);
	UDR=data>>8;
	while(!(UCSRA&(1<<UDRE)));
	_delay_ms(10);
	UDR=data;
	DDRD&=0XFD;
	_delay_ms(10);
}

void UARTSend1(long int data) {
	DDRD|=0X02;//TXD
	UDR=data;
	while(!(UCSRA&(1<<UDRE)));

	DDRD&=0XFD;
	_delay_ms(10);
}

void USART_SendChar(unsigned char sym)
{
	_delay_us(10);
	while(!(UCSRA & (1<<UDRE)))
	_delay_us(10);
	UDR = sym;
}

void USART_SendStr(char * s)
{
	_delay_ms(1);
	while(*(s)!='\0')
	{
	USART_SendChar(*(s++));
	}
}

void send_as_str(unsigned int data)
{
	char bufer[12];
	sprintf(bufer, "%u",  data );
	USART_SendStr(bufer);
	USART_SendChar(13);
	USART_SendChar(10);
}

void float_as_str(float data)
{
	char bufer[12];
	sprintf(bufer,"%.2f", data);
	USART_SendStr(bufer);
	USART_SendChar(13);
	USART_SendChar(10);
}

void hit_sensor(void)
{
	/*	if (ACCEL>0xCA00)
					{
						status=0x55;
						hit_counter++;
						message_counter=0;
					}
			if (message_counter>10)
			{
				message_counter=0;
				status=0xff;
			}//*/
}


int main(void)
{
	init_devices();
	sei();
	buffer_index=0;		// Сбрасываем индекс
	//UDR = DATA[0];
	//UCSRB|=(1<<UDRIE);// Отправляем первый байт
	start_flag=0;
	while(1)
	{

		switch (ADC_status)
		         {
		                case 0:
		                        ReadADC_PRESSURE();
		                        DATA[2]=buffer_send[2];
		                        DATA[3]=buffer_send[3];
		                        ADC_status=1;
		                        break;

		                case 1:
		                		ReadADC_C();
		                        DATA[4]=buffer_send[4];
		                        DATA[5]=buffer_send[5];
		                        ADC_status=2;
		                        break;


		                case 2:
		                        ReadADC_A();
		                        DATA[6]=buffer_send[6];
		                        DATA[7]=buffer_send[7];
		                        ADC_status=3;
		                        break;

		                case 3:
		                        ReadADC_B();
		                        DATA[8]=buffer_send[8];
		                        DATA[9]=buffer_send[9];
		                        ADC_status=4;
		                        break;

		                case 4:
		                		//cli();
		                        PORTD&=0XDf;
		                        _delay_us(2);
		                        ADC_status=0;
		                        PORTD|=0X20;//SET-RESET
		                        //sei();
		                        break;

		                default:  ADC_status=0;  break;
		          }

		COMPAS_X=DATA[7]+(DATA[6]<<8);
		COMPAS_Y=DATA[9]+(DATA[8]<<8);
		COMPAS_Z=DATA[5]+(DATA[4]<<8);

		   if (start_flag == 0)
		         {

		         	i++;
		         	if (i==10)
		         	{
		         		max_Z=COMPAS_Z;
		         		UARTSend(max_Z);

		         		_delay_ms(5000);
		         	}
		         	if (i>10){
		         	if (i<250)
		         	{

		         		if (COMPAS_X>max_X)
		         		{
		         		max_X=COMPAS_X;
		         		}
		         		if (COMPAS_X<min_X)
		         		{
		         		min_X=COMPAS_X;
		         		}
		         		if (COMPAS_Y>max_Y)
		         				{
		         				max_Y=COMPAS_Y;
		         				}
		         				if (COMPAS_Y<min_Y)
		         				{
		         				min_Y=COMPAS_Y;
		         				}

		         				UARTSend(COMPAS_X);
		         	}
		         	else
		         	{
		         		_delay_ms(5000);
		         		min_Z=COMPAS_Z;
		         		UARTSend(min_Z);
		         		_delay_ms(5000);
		         		UARTSend(0xff);
		         		start_flag = 1;
		         		UARTSend(max_X);
		         		_delay_ms(10);
		         		UARTSend(min_X);
		         		_delay_ms(10);
		         		UARTSend(min_Y);
		         		_delay_ms(10);
		         		UARTSend(max_Y);
		         		_delay_ms(10);
		         		UARTSend(0xff);

		         		_delay_ms(5000);

		         	}
		         }
		         }

		if (start_flag==1)
		   		{
		a=COMPAS_X-min_X-((max_X-min_X)/2);//signed x
		b=COMPAS_Y-min_Y-((max_Y-min_Y)/2);
		g=COMPAS_Z-min_Z-((max_Z-min_Z)/2);

		if ((max_X-min_X)>(max_Y-min_Y))
				        {
				if ((max_X-min_X)>(max_Z-min_Z))
				{
				        		b=b*((max_X-min_X)/(max_Y-min_Y));
								g=g*((max_X-min_X)/(max_Z-min_Z));
				}
				else
				{
					a=a*((max_Z-min_Z)/(max_X-min_X));
					b=b*((max_Z-min_Z)/(max_Y-min_Y));
				}
				        }
				        		else
				        		{
				        			if ((max_Y-min_Y)>(max_Z-min_Z))
				        		{
				        			a=a*((max_Y-min_Y)/(max_X-min_X));
				        			g=g*((max_Y-min_Y)/(max_Z-min_Z));
				        		}
				        			else
				        			{
				        				a=a*((max_Z-min_Z)/(max_X-min_X));
				        				b=b*((max_Z-min_Z)/(max_Y-min_Y));
				        			}
				        		}
		   		}





		_delay_ms(40);
		start_accel();
		GET_ACCEL ();
		GET_GYRO();
		GET_TEMPER();

		ACCEL_XANGLE=atan2(-(float)ACCEL_X, (float)sqrt(pow(ACCEL_Y, 2)+pow(ACCEL_Z,2)));
		ACCEL_YANGLE=atan2((float)ACCEL_Y, (float)sqrt(pow(ACCEL_X, 2)+pow(ACCEL_Z,2)));
		ACCEL_ZANGLE=atan2((float)ACCEL_Z, (float)sqrt(pow(ACCEL_X, 2)+pow(ACCEL_Y,2)));
		ACCEL=sqrt(pow(ACCEL_X, 2)+pow(ACCEL_Y, 2)+pow(ACCEL_Z,2));
		hit_sensor();

		DATA[0]=0xBB;
		DATA[1]=status;
		DATA[12]=ACCEL_xh;
		DATA[13]=ACCEL_xl;
		DATA[14]=ACCEL_yh;
		DATA[15]=ACCEL_yl;
		DATA[16]=ACCEL_zh;
		DATA[17]=ACCEL_zl;
		DATA[18]=GYRO_xh;
		DATA[19]=GYRO_xl;
		DATA[20]=GYRO_yh;
		DATA[21]=GYRO_yl;
		DATA[22]=GYRO_zh;
		DATA[23]=GYRO_zl;
		DATA[24]=pitch_h;
		DATA[25]=pitch_l;
		DATA[26]=roll_h;
		DATA[27]=roll_l;


		roll=180/M_PI*ACCEL_XANGLE;
		pitch=180/M_PI*ACCEL_YANGLE;
		if ((ACCEL_X>0)&(ACCEL_Z>0))
		{
			//send_as_str(360+pitch);
			roll=360+roll;
			ACCEL_XANGLE=ACCEL_XANGLE+2*pi;

		}
		if ((ACCEL_X>0)&(ACCEL_Z<0))
				{
					//send_as_str(180-pitch);
			roll=180-roll;
			ACCEL_XANGLE=pi-ACCEL_XANGLE;
				}
		if ((ACCEL_X<0)&(ACCEL_Z<0))
				{
					//send_as_str(180-pitch);
			roll=180-roll;
			ACCEL_XANGLE=pi-ACCEL_XANGLE;
				}
		if ((ACCEL_X<0)&(ACCEL_Z>0))
						{
							//send_as_str(pitch);
						}

		if ((ACCEL_Y>0)&(ACCEL_Z>0))
				{
					//send_as_str(360-roll);
			pitch=360-pitch;
			ACCEL_YANGLE=2*pi-ACCEL_YANGLE;
				}
		if ((ACCEL_Y>0)&(ACCEL_Z<0))
				{
					//send_as_str(180+roll);
			pitch=180+pitch;
			ACCEL_YANGLE=pi+ACCEL_YANGLE;
				}
		if ((ACCEL_Y<0)&(ACCEL_Z<0))
				{
					//send_as_str(180+roll);
			pitch=180+pitch;
			ACCEL_YANGLE=pi+ACCEL_YANGLE;
				}
		if ((ACCEL_Y<0)&(ACCEL_Z>0))
				{
					//send_as_str(-roll);
			pitch=-pitch;
			ACCEL_YANGLE=-ACCEL_YANGLE;
				}

		pitch_h=pitch>>8;
		pitch_l=pitch;

		roll_h=roll>>8;
		roll_l=roll;

		if (start_flag==1)
		{
			a1=a*cos(pitch)+b*sin(roll)*sin(pitch)+g*cos(roll)*sin(pitch);
			b1=b*cos(roll)-g*sin(roll);

			//a1=a*cos(pitch)+b*sin(pitch)*sin(roll)+g*sin(pitch)*cos(roll);
			//b1=b*cos(roll)-g*cos(roll);

			 A+=a1;
						          B+=b1;
						         if (compass_average ==14) {compass_average=0;
						                                  A=A/15;
						                                  B=B/15;
						                                  gradus=57.295*atan2(-(double)B,-(double)A)+180;
						                                  o=gradus;
						                                // ..ACCEL_XANGLE=57.295*atan2(-(float)ACCEL_Y, -(float)ACCEL_Z)+180;
						                                 // UARTSend(A);
						                                  sprintf(bufer, "%u",  o );
						                                  USART_SendStr(bufer);
						                                  USART_SendChar(13);
						                                  USART_SendChar(10);
						                                  A=0;
						                                  B=0;
						                                    }
						         else compass_average++;
								//UARTSend(gradus);


							}

		//UARTSend(0xFF);
		//_delay_ms(10);
		//send_as_str(roll);
		//send_as_str(pitch);
		//UARTSend(0xFFFF);
	}
}
