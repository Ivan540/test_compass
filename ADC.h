
//#include <iom8.h>
//#include <inavr.h>
//#define	DRDY		(PINE & 0X10)					// ADC Data Ready (active low, ADC master)
//#define	RSTADC		PE5							// ADC Reset (active low, ADC slave)
												// SPI
#define	MOSI		PB3
#define	SCK		PB5

#define CommWR			0
#define CommRD			64
#define CommMode		1
#define CommControl		2
#define CommFilter		3
#define CommData		4
#define CommOffset		5
#define CommGain		6
#define CommIO			7
#define CommTest1		12
#define CommTest2		13
#define CommID			15
												// AD7718 variables
												// ADC update rate for CHOP disabled
#define updateRateNoCHOP1365Hz	3				// ADC update rate = 1365.33	Hz
#define updateRateNoCHOP315Hz	13				// ADC update rate = 315		Hz
#define updateRateNoCHOP59Hz	69				// ADC update rate = 59.36		Hz
#define updateRateNoCHOP16Hz	255				// ADC update rate = 16.06		Hz
												// ADC update rate for CHOP enabled
#define updateRateCHOP105Hz		13				// ADC update rate = 105.3		Hz
#define updateRateCHOP59Hz		23				// ADC update rate = 59.36		Hz
#define updateRateCHOP51Hz		27				// ADC update rate = 50.56		Hz
#define updateRateCHOP30Hz		45				// ADC update rate = 30.3		Hz

#define BaseModeNoCHOP			147				// Chopping is disabled, AINCOM is unbuffered allowing it to be tied to AGND in single-ended input configuration, configured as a 10 pseudodifferential input, oscillator is not shut off, continuous conversion
#define BaseModeNoCHOP_S		146	//single
#define BaseModeCHOP			19				// Chopping is enabled, AINCOM is unbuffered allowing it to be tied to AGND in single-ended input configuration, configured as a 10 pseudodifferential input, oscillator is not shut off, continuous conversion
#define BaseModeCHOP_S			18//single
#define BaseControl				15// Select input AIN1, enable unipolar coding, select the ADC input range as 0...+5 V
#define CControl				128//137// +80mV Select input AIN1&AIN2, enable unipolar coding, select the ADC input range as 0...+80mV
#define PressControl				191//+320mv//186//+160mV Select input AIN7&AIN8, enable bipolar coding, select the ADC input range as +160mV
#define BControl				192//+40// Select input AIN9&AIN10, enable unipolar coding, select the ADC input range as 0...+80mV
#define AControl				144// Select input AIN3&AIN4, enable unipolar coding, select the ADC input range as 0...+80mV
#define accelerXnoFch	1										// AD7718 channels
volatile char buffer_send[20];


void __delay_cycles(long int a)
{
long int GG;

for (GG=0;GG<(a-1);GG++);


}

void initMasterSPI(void)						// SPI initialization as a Master
{DDRB |=(0x04) ;	
DDRB |= 0x02 ;// Set MOSI and SCK output, all others input
PORTB|=0X01;//SS set high
DDRB|=0X01;
	DDRB |=0x08 ;	
	DDRB|=0X20;
	SPCR = ( 1 << SPE ) | ( 1 << MSTR )| ( 1 << SPR0 ) ;// Enable SPI, Master, set clock rate fck/16
}//

void transmitMasterSPI(unsigned char SPIData )	// Perform a SPI transmission
{
	SPDR = SPIData;								// Start transmission 
	while( !( SPSR & ( 1 << SPIF )));			// Wait for transmission complete 
	SPSR&=0X7F;
}


void configADC (void)							// Configure ADC
{char i;
    
	 for (i=0;i<4;i++) _delay_ms(40);//__delay_cycles( 320000 );
	transmitMasterSPI( CommWR | CommFilter );
	transmitMasterSPI( updateRateCHOP105Hz );
	for (i=0;i<4;i++) _delay_ms(40);//__delay_cycles( 320000 );
	transmitMasterSPI( CommWR | CommMode );
	transmitMasterSPI( BaseModeCHOP_S );	
	for (i=0;i<4;i++) _delay_ms(40);//__delay_cycles( 320000 );
	transmitMasterSPI( CommWR | CommControl );
	transmitMasterSPI( PressControl );
	for (i=0;i<4;i++) _delay_ms(40);//__delay_cycles( 320000 );
	   transmitMasterSPI( CommWR | CommIO );
	transmitMasterSPI( 0x32);
	
	
	 for (i=0;i<18;i++) _delay_ms(40);//__delay_cycles( 320000 );
	 
	//while ( DRDY ) ;
}
void setChannel (unsigned char channel)			// Set ADC channel
{
	channel <<= 4;
	
	transmitMasterSPI( CommWR | CommControl );
	transmitMasterSPI( channel | BaseControl );
	
	//while ( DRDY ) ;
}
void ReadADC_PRESSURE (void)								// ADC reading
{//char i;
		char a;				// ADC current reading
	//while ( DRDY ) ;
	//for (i=0;i<18;i++) __delay_cycles( 320000 );
	transmitMasterSPI( CommRD | CommData );
	transmitMasterSPI( 0xFF );
	buffer_send[2]= SPDR;
	transmitMasterSPI( 0xFF );
	buffer_send[3]= SPDR;
	transmitMasterSPI( 0xFF );
	a = SPDR;

	transmitMasterSPI( CommWR | CommControl );
	transmitMasterSPI( CControl );
	
	
	transmitMasterSPI( CommWR | CommMode );
	transmitMasterSPI( BaseModeCHOP_S );	
	
}
void ReadADC_C (void)								// ADC reading
{//char i;
		char a;				// ADC current reading
	//while ( DRDY ) ;
	//for (i=0;i<18;i++) __delay_cycles( 320000 );
	transmitMasterSPI( CommRD | CommData );
	transmitMasterSPI( 0xFF );
	buffer_send[4]= SPDR;
	transmitMasterSPI( 0xFF );
	buffer_send[5]= SPDR;
	transmitMasterSPI( 0xFF );
	a = SPDR;
	
	transmitMasterSPI( CommWR | CommControl );
	transmitMasterSPI( AControl );
	//transmitMasterSPI( PressControl );
	transmitMasterSPI( CommWR | CommMode );
	transmitMasterSPI( BaseModeCHOP_S );	
	
}



void ReadADC_A (void)								// ADC reading
{//char i;
		char a;				// ADC current reading
	//while ( DRDY ) ;
	//for (i=0;i<18;i++) __delay_cycles( 320000 );
	transmitMasterSPI( CommRD | CommData );
	transmitMasterSPI( 0xFF );
	buffer_send[6]= SPDR;
	transmitMasterSPI( 0xFF );
	buffer_send[7]= SPDR;
	transmitMasterSPI( 0xFF );
	a = SPDR;
	
	transmitMasterSPI( CommWR | CommControl );
	transmitMasterSPI( BControl );
	
	transmitMasterSPI( CommWR | CommMode );
	transmitMasterSPI( BaseModeCHOP_S );	
	
}



void ReadADC_B (void)								// ADC reading
{//char i;
		char a;				// ADC current reading
	//while ( DRDY ) ;
	//for (i=0;i<18;i++) __delay_cycles( 320000 );
	transmitMasterSPI( CommRD | CommData );
	transmitMasterSPI( 0xFF );
	buffer_send[8]= SPDR;
	transmitMasterSPI( 0xFF );
	buffer_send[9]= SPDR;
	transmitMasterSPI( 0xFF );
	a = SPDR;
	
	transmitMasterSPI( CommWR | CommControl );
	transmitMasterSPI( PressControl );
	
	
	transmitMasterSPI( CommWR | CommMode );
	transmitMasterSPI( BaseModeCHOP_S );	
	
}


